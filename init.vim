""""""""""""""""""""""""""""""
" プラグインのセットアップ
""""""""""""""""""""""""""""""
" プラグインが実際にインストールされるディレクトリ
let s:dein_dir = expand('~/.config/nvim/dein')
let s:toml_dir = expand('~/.config/nvim/')
" dein.vim 本体
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

" dein.vim がなければ github から落としてくる
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif


" 設定開始
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  call dein#add('Shougo/dein.vim')

  let s:toml      = s:toml_dir . '/dein.toml'
  let s:lazy_toml = s:toml_dir . '/dein_lazy.toml'
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})

  " 設定終了
  call dein#end()
  call dein#save_state()
endif

" もし、未インストールものものがあったらインストール
if dein#check_install()
  call dein#install()
endif

" Required:
filetype plugin indent on

""""""""""""""""""""""""""""""
" vimのセットアップ
""""""""""""""""""""""""""""""
" 英語メニューに
set langmenu=en_US
let $LANG = 'en_US'
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
scriptencoding utf-8
set bomb
set binary
set ttyfast

"" Fix backspace indent
set backspace=indent,eol,start

" XML,HTML,ERBのタグ閉じ補完
augroup MyXML
  autocmd!
  autocmd Filetype xml inoremap <buffer> </ </<C-x><C-o>
  autocmd Filetype html inoremap <buffer> </ </<C-x><C-o>
  autocmd Filetype eruby inoremap <buffer> </ </<C-x><C-o>
augroup END

" indentLineの設定 tabもindentLineに
set list listchars=tab:\¦\
" swapfileを使用しない
set noswapfile
set nobackup
" Buffer未保存でも別ファイルを開ける
set hidden
" タイトルバーにファイルのパス情報等を表示する
set title
" コマンドラインモードで<Tab>キーによるファイル名補完を有効にする
set wildmenu
" 入力中のコマンドを表示する
set showcmd
" 検索結果をハイライト表示する
set hlsearch
" ESCを二回押すことでハイライトを消す
nmap <silent> <Esc><Esc> :nohlsearch<CR>
" タブ入力を複数の空白入力に置き換える
set expandtab
" 小文字のみで検索したときに大文字小文字を無視する
set smartcase
"新しい行のインデントを現在行と同じにする
set autoindent
"タブ幅の設定"
set expandtab
set tabstop=2
set shiftwidth=2
" フォント
set guifont=Ricty-Regular:h12"
set guifontwide=Ricty-Regular:h12"

" vimgrep時に自動的にquickfix-windowを表示
autocmd QuickFixCmdPost *grep* cwindow


"Python3 support
let g:python3_host_prog ='/usr/local/bin/python3'
let g:python_host_prog ='/usr/local/bin/python2'

""""""""""""""""""""""""""""""
" Visual Settings
""""""""""""""""""""""""""""""
" Syntax
syntax on

"行番号を表示する
set number
set ruler

"閉括弧が入力された時、対応する括弧を強調する
set showmatch

" 対応する括弧のハイライト調節
hi MatchParen ctermbg=1

" 対応する括弧のハイライト非表示
" let loaded_matchparen = 1


"不可視文字を表示"
"set list
"set listchars=tab:»-,trail:-,eol:↲,extends:»,precedes:-,nbsp:%
"set listchars=tab:»-,eol:↲,extends:»,precedes:-,nbsp:%

"" Disable the blinking cursor.
" set gcr=a:blinkon0
" set scrolloff=3

"" Status bar
set laststatus=2

"" Use modeline overrides
set modeline
set modelines=10

" set title
set titleold="Terminal"
set titlestring=%F

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" .vueシンタックスハイライト
autocmd BufNewFile,BufRead *.vue set filetype=html
" 全角スペースのハイライト
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=darkgray
endfunction
if has('syntax')
    augroup ZenkakuSpace
        autocmd!
        autocmd ColorScheme * call ZenkakuSpace()
        autocmd VimEnter,WinEnter,BufRead * let w:m1=matchadd('ZenkakuSpace', '　')
    augroup END
    call ZenkakuSpace()
endif

" .ejsファイルのシンタックスをhtmlに
au BufNewFile,BufRead *.ejs set filetype=html

" HTML 閉じタグ補完
augroup MyXML
  autocmd!
  autocmd Filetype xml inoremap <buffer> </ </<C-x><C-o>
  autocmd Filetype html inoremap <buffer> </ </<C-x><C-o>
augroup END

" 補完
" set completeopt=menuone
" for k in split("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_",'\zs')
"   exec "imap <expr> " . k . " pumvisible() ? '" . k . "' : '" . k . "\<C-X>\<C-P>\<C-N>'"
" endfor

""""""""""""""""""""""""""""""
" Mappings
""""""""""""""""""""""""""""""
"キーバインド切り替え"
" nnoremap ww :<C-u>vs<CR> "垂直分割
" nnoremap wh :<C-u>sp<CR> "水平分割
" nnoremap wn <C-w>w "次のウィンドウに移動
" nnoremap tt :<C-u>tabnew<CR> "新規タブ
" nnoremap tn gt "次のタブへ切り替え
" nnoremap tp gT "前のタブへ切り替え

" カーソル移動矯正
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" 新しいタブでターミナルを起動
" nnoremap <C-t> :tabe<CR>:terminal<CR>
" 直前のバッファを開く
nnoremap <C-q> <C-^><CR>
" ESCでターミナルモードからノーマルモードへ
tnoremap <ESC> <C-\><C-n>
