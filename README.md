# nvim
nvim conf

# Add .bashrc
```
# Neovim config
export XDG_CONFIG_HOME="$HOME/.config"
```

# Create Config Dir
```
$ mkdir ~/.config
$ mkdir ~/.config/nvim
```
